﻿package model;

public class BigDog extends Dog {

    public BigDog(String name) {
        super(name);

    }

    @Override
    public void greets() {
        System.out.println("Woow");
    }

    public void greets(Dog anotherDog) {
        System.out.println("Wooooow");
    }

    public void greets(BigDog anotherBigDog) {
        System.out.println("Wooooooooow");
    }

}
