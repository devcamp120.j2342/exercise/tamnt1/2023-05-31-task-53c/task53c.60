
public class App {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Fluffy");
        Dog dog = new Dog("Buddy");
        BigDog bigDog = new BigDog("Max");

        cat.greets();
        dog.greets();
        bigDog.greets();
    }
}
